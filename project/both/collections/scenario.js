Scenario = new Mongo.Collection('scenario');

Scenario.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  scenarioDescription:{
    type:String
  },
  npCharacterIds:{
    type:[String],
    optional: true
  },
  item:{
    type:[String]
  }
  
}));

/*
 * Add query methods like this:
 *  Scenario.findPublic = function () {
 *    return Scenario.find({is_public: true});
 *  }
 */