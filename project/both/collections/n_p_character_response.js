NPCharacterResponse = new Mongo.Collection('n_p_character_response');

NPCharacterResponse.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  scenarioIds : {
    type:[String],
    optional: true
  },
  requestIds : {
    type:[String],
    optional: true
  },
  speech : {
    type:[String]
  }

}));
/*
 * Add query methods like this:
 *  NPCharacterResponse.findPublic = function () {
 *    return NPCharacterResponse.find({is_public: true});
 *  }
 */